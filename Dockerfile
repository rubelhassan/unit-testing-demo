FROM openjdk:8-jdk-alpine

VOLUME /tmp

ARG JAR_FILE
ARG APP_ENV
RUN echo "App Env:" + "$APP_ENV"
COPY ${JAR_FILE} app.jar

# export port for remote debugging
EXPOSE 5005

ENV APP_ENV_VAL=$APP_ENV

ENTRYPOINT "java" \
    "-agentlib:jdwp=transport=dt_socket,address=5005,suspend=n,server=y" \
    "-jar" \
    "-Dspring.profiles.active=$APP_ENV_VAL" \
    "/app.jar"