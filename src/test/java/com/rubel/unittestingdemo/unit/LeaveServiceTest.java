package com.rubel.unittestingdemo.unit;

import com.rubel.unittestingdemo.exception.LeaveLimitExceededException;
import com.rubel.unittestingdemo.exception.LeaveNotFoundException;
import com.rubel.unittestingdemo.model.Employee;
import com.rubel.unittestingdemo.model.Leave;
import com.rubel.unittestingdemo.repository.LeaveRepository;
import com.rubel.unittestingdemo.service.NotificationService;
import com.rubel.unittestingdemo.service.impl.LeaveServiceImpl;
import com.rubel.unittestingdemo.utils.LeaveStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

/**
 * @author Rubel Hassan
 * @since 11/4/18
 */
@DisplayName("Leave Service Implementation Test")
@ExtendWith(MockitoExtension.class)
public class LeaveServiceTest {
    @InjectMocks
    LeaveServiceImpl leaveService;

    @Mock
    LeaveRepository leaveRepository;

    @Mock
    NotificationService notificationService;

    private Leave fakeLeave;
    private Employee fakeEmployee;

    @BeforeEach
    void setUp(){
        fakeEmployee = new Employee(1l, "Rubel Hassan", 20);
        fakeLeave = new Leave(2l, "Fake Reason", 3,
                LocalDate.of(2018, 11, 1),
                LocalDate.of(2018, 11, 3), fakeEmployee, LeaveStatus.PENDING);
    }

    @Test
    @DisplayName("Test pending leave of an employee with available leaves count is being approved")
    void testApproveLeave() throws LeaveLimitExceededException, LeaveNotFoundException {
        when(leaveRepository.findById(fakeLeave.getId()))
                .thenReturn(Optional.of(fakeLeave));

        leaveService.approveLeave(fakeLeave.getId());

        assertThat(fakeEmployee.getAvailableLeavesCount(), is(17));
        assertThat(fakeLeave.getStatus(), is(LeaveStatus.APPROVED));
    }

    @Test
    @DisplayName("Test approved leave notification is sent")
    void testApproveLeaveNotificationSent() throws LeaveLimitExceededException, LeaveNotFoundException {
        when(leaveRepository.findById(fakeLeave.getId()))
                .thenReturn(Optional.of(fakeLeave));

        leaveService.approveLeave(fakeLeave.getId());

        verify(leaveRepository, times(1)).save(fakeLeave);
        verify(notificationService, times(1)).notifyLeaveApproved(fakeLeave);
    }

    @Test
    @DisplayName("Test pending leave of an employee with no leaves should not be approved")
    void testApproveLeaveWithNoLeaves(){
        fakeEmployee.setAvailableLeavesCount(0);
        when(leaveRepository.findById(fakeLeave.getId()))
                .thenReturn(Optional.of(fakeLeave));

        assertThrows(LeaveLimitExceededException.class, ()-> leaveService.approveLeave(fakeLeave.getId()),
                "LeaveLimitExceededException must be throw if employee has no leaves left while approving leave");

        verify(leaveRepository).findById(fakeLeave.getId());
    }

    @Test
    @DisplayName("Test pending leave of an employee with less leaves available should not be approved")
    void testApproveLeaveWithLessLeaves(){
        fakeEmployee.setAvailableLeavesCount(2);
        when(leaveRepository.findById(fakeLeave.getId()))
                .thenReturn(Optional.of(fakeLeave));

        assertThrows(LeaveLimitExceededException.class, ()-> leaveService.approveLeave(fakeLeave.getId()),
                "LeaveLimitExceededException must be throw if employee has less leaves left while approving leave");

        verify(leaveRepository).findById(fakeLeave.getId());
    }

    @Test
    @DisplayName("Test pending leave of an employee with invalid id should throw exception")
    void testApproveLeaveWithInvalidLeave(){
        when(leaveRepository.findById(fakeLeave.getId()))
                .thenReturn(Optional.empty());

        assertThrows(LeaveNotFoundException.class, ()-> leaveService.approveLeave(fakeLeave.getId()),
                "LeaveNotFoundException must be throw if leave id is invalid");

        verify(leaveRepository).findById(fakeLeave.getId());
        verify(leaveRepository, never()).save(fakeLeave);
    }
}
