package com.rubel.unittestingdemo.integration;

import com.rubel.unittestingdemo.UnitTestingDemoApplication;
import com.rubel.unittestingdemo.config.TestConfig;
import com.rubel.unittestingdemo.model.Employee;
import com.rubel.unittestingdemo.model.Leave;
import com.rubel.unittestingdemo.utils.LeaveStatus;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;
import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

/**
 * @author Rubel Hassan
 * @since 11/4/18
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {UnitTestingDemoApplication.class})
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class LeaveControllerIntegrationTest {
    @Autowired
    MockMvc mockMvc;

    private MediaType contentType = new MediaType("application", "hal+json", Charset.forName("UTF-8"));

    @Test
    void tetGetLeaveEntity() throws Exception {
        Employee expectedEmployee = new Employee(1l, "Rubel Hassan", 20);
        Leave expectedLeave = new Leave(2l, "Test Reason 1", 1,
                LocalDate.of(2018, 11, 1),
                LocalDate.of(2018, 11, 1), expectedEmployee, LeaveStatus.PENDING);

        mockMvc.perform(get("/leaves/2"))
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.leave.reason", is(expectedLeave.getReason())))
                .andExpect(jsonPath("$.leave.count", is(expectedLeave.getCount())))
                .andExpect(jsonPath("$.leave.start", is(expectedLeave.getStart().toString())))
                .andExpect(jsonPath("$.leave.finish", is(expectedLeave.getFinish().toString())))
                .andExpect(jsonPath("$.leave.status", is(expectedLeave.getStatus().toString())));
    }
}
