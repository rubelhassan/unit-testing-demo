package com.rubel.unittestingdemo.controller;

import com.rubel.unittestingdemo.exception.LeaveLimitExceededException;
import com.rubel.unittestingdemo.exception.LeaveNotFoundException;
import com.rubel.unittestingdemo.model.Leave;
import com.rubel.unittestingdemo.resource.LeaveResource;
import com.rubel.unittestingdemo.service.LeaveService;
import com.rubel.unittestingdemo.validator.LeaveValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * @author Rubel Hassan
 * @since 10/31/18
 */
@RestController
@RequestMapping(value = "/leaves", produces = "application/hal+json")
public class LeaveController {

    private LeaveService leaveService;
    private LeaveValidator leaveValidator;

    @GetMapping
    public ResponseEntity<PagedResources<Resource<LeaveResource>>> all(Pageable pageable,
                                                                          PagedResourcesAssembler<LeaveResource> assembler) {
        final Page<Leave> leaves = leaveService.getLeaves(pageable);
        final Page<LeaveResource> leaveResources = leaves.map(LeaveResource::new);
        final PagedResources<Resource<LeaveResource>> resources = assembler.toResource(leaveResources);
        return ResponseEntity.ok(resources);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> get(@PathVariable("id") Long id) throws LeaveNotFoundException {
        return ResponseEntity.ok(new LeaveResource(leaveService.getLeave(id)));
    }

    @PostMapping
    public ResponseEntity<LeaveResource> create(@RequestBody @Valid Leave leave) {
        leaveService.createLeave(leave);
        final URI uri = linkTo(LeaveController.class).slash(leave.getId()).toUri();
        return ResponseEntity.created(uri).body(new LeaveResource(leave));
    }

    @PutMapping("/{id}")
    public ResponseEntity<LeaveResource> update(@PathVariable("id") Long id,
                                                   @RequestBody Leave leave) throws LeaveNotFoundException {
        leaveService.updateLeave(id, leave);
        final URI uri = linkTo(LeaveController.class).slash(leave.getId()).toUri();
        return ResponseEntity.created(uri).body(new LeaveResource(leave));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) throws LeaveNotFoundException {
        leaveService.deleteLeave(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{id}/approve")
    public ResponseEntity<?> approve(@PathVariable("id") Long id)
            throws LeaveNotFoundException, LeaveLimitExceededException {
        return ResponseEntity.ok(new LeaveResource(leaveService.approveLeave(id)));
    }

    @PostMapping("/{id}/deny")
    public ResponseEntity<?> deny(@PathVariable("id") Long id) throws LeaveNotFoundException {
        return ResponseEntity.ok(new LeaveResource(leaveService.denyLeave(id)));
    }

    @Autowired
    public void setLeaveService(LeaveService leaveService) {
        this.leaveService = leaveService;
    }

    @Autowired
    public void setLeaveValidator(LeaveValidator leaveValidator) {
        this.leaveValidator = leaveValidator;
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder){
        binder.setValidator(leaveValidator);
    }
}
