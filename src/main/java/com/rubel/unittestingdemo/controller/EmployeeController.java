package com.rubel.unittestingdemo.controller;

import com.rubel.unittestingdemo.exception.LeaveNotFoundException;
import com.rubel.unittestingdemo.model.Leave;
import com.rubel.unittestingdemo.resource.LeaveResource;
import com.rubel.unittestingdemo.service.LeaveService;
import com.rubel.unittestingdemo.validator.LeaveValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * @author Rubel Hassan
 * @since 10/31/18
 */
@RestController
@RequestMapping(value = "/employees", produces = "application/hal+json")
public class EmployeeController {
}
