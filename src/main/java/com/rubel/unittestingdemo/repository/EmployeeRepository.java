package com.rubel.unittestingdemo.repository;

import com.rubel.unittestingdemo.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Rubel Hassan
 * @since 10/30/18
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
