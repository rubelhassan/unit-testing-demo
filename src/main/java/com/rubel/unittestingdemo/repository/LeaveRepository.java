package com.rubel.unittestingdemo.repository;

import com.rubel.unittestingdemo.model.Leave;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Rubel Hassan
 * @since 10/30/18
 */
public interface LeaveRepository extends JpaRepository<Leave, Long> {
}
