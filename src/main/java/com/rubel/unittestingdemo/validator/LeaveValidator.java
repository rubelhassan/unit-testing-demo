package com.rubel.unittestingdemo.validator;

import com.rubel.unittestingdemo.model.Leave;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;
import java.util.concurrent.TimeUnit;

/**
 * @author Rubel Hassan
 * @since 10/31/18
 */
@Component
public class LeaveValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Leave.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Leave leave = (Leave) target;
        ValidationUtils.rejectIfEmpty(errors, "reason", "leave.reason.empty");

        if(leave.getEmployee() == null || leave.getEmployee().getId() == null){
            errors.rejectValue("employee", "leave.employee.null");
        }

        if(leave.getCount() != null && leave.getCount() <= 0){
            errors.rejectValue("count", "leave.count.positive");
        }

        if(leave.getStart() == null){
            errors.rejectValue("start", "leave.start.null");
        }

        if(leave.getFinish() == null){
            errors.rejectValue("finish", "leave.finish.null");
        }

        if(leave.getStart().compareTo(leave.getFinish()) > 0){
            errors.rejectValue("start", "leave.start.before");
        }

        int leaveDurationInDays = Period.between(leave.getStart(), leave.getFinish()).getDays();

        if(leaveDurationInDays < 0){
            errors.rejectValue("count", "leave.count.duration");
        }

        if(leaveDurationInDays + 1 != leave.getCount()){
            errors.rejectValue("count", "leave.count.mismatch");
        }
    }
}
