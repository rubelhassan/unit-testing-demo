package com.rubel.unittestingdemo.config;

import com.rubel.unittestingdemo.model.Employee;
import com.rubel.unittestingdemo.model.Leave;
import com.rubel.unittestingdemo.repository.EmployeeRepository;
import com.rubel.unittestingdemo.repository.LeaveRepository;
import com.rubel.unittestingdemo.utils.LeaveStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author Rubel Hassan
 * @since 11/3/18
 */
@Component
@Profile("test")
public class TestDataLoader implements CommandLineRunner {

    LeaveRepository leaveRepository;
    EmployeeRepository employeeRepository;

    @Override
    public void run(String... args) {
        Employee employee = new Employee(1l, "Rubel Hassan", 20);
        List<Leave> leaves = creteTestLeavesForEmployee(employee, new AtomicLong(2));
        employeeRepository.save(employee);
        leaveRepository.saveAll(leaves);
    }

    @Autowired
    public void setLeaveRepository(LeaveRepository leaveRepository) {
        this.leaveRepository = leaveRepository;
    }

    @Autowired
    public void setEmployeeRepository(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    List<Leave> creteTestLeavesForEmployee(Employee employee, AtomicLong id){
        List<Leave> leaves = new ArrayList<>();
        leaves.add(new Leave(id.incrementAndGet(), "Test Reason 1", 1,
                LocalDate.of(2018, 11, 1),
                LocalDate.of(2018, 11, 1), employee, LeaveStatus.PENDING));
        leaves.add(new Leave(id.incrementAndGet(), "Test Reason 2", 2,
                LocalDate.of(2018, 11, 7),
                LocalDate.of(2018, 11, 8), employee, LeaveStatus.PENDING));
        leaves.add(new Leave(id.incrementAndGet(), "Test Reason 3", 4,
                LocalDate.of(2018, 11, 20),
                LocalDate.of(2018, 11, 23), employee, LeaveStatus.PENDING));
        return leaves;
    }
}
