package com.rubel.unittestingdemo.service;

import com.rubel.unittestingdemo.model.Leave;

/**
 * @author Rubel Hassan
 * @since 11/2/18
 */
public interface AccountService {
    void notifyLeaveApproved(Leave leave);
}
