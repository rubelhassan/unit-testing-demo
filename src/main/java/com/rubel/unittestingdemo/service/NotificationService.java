package com.rubel.unittestingdemo.service;

import com.rubel.unittestingdemo.model.Leave;

/**
 * @author Rubel Hassan
 * @since 11/3/18
 */
public interface NotificationService {
    void notifyLeaveApproved(Leave leave);
}
