package com.rubel.unittestingdemo.service.impl;

import com.rubel.unittestingdemo.exception.LeaveLimitExceededException;
import com.rubel.unittestingdemo.exception.LeaveNotFoundException;
import com.rubel.unittestingdemo.model.Employee;
import com.rubel.unittestingdemo.model.Leave;
import com.rubel.unittestingdemo.repository.EmployeeRepository;
import com.rubel.unittestingdemo.repository.LeaveRepository;
import com.rubel.unittestingdemo.service.AccountService;
import com.rubel.unittestingdemo.service.LeaveService;
import com.rubel.unittestingdemo.service.NotificationService;
import com.rubel.unittestingdemo.utils.LeaveStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * @author Rubel Hassan
 * @since 10/30/18
 */
@Service
@Transactional
public class LeaveServiceImpl implements LeaveService {

    private LeaveRepository leaveRepository;
    private NotificationService notificationService;

    @Override
    public Leave approveLeave(Long id) throws LeaveLimitExceededException, LeaveNotFoundException {
        Leave leave = getLeave(id);
        Employee applicant = leave.getEmployee();

        if(applicant.getAvailableLeavesCount() >= leave.getCount()){
            leave.setStatus(LeaveStatus.APPROVED);
            applicant.setAvailableLeavesCount(applicant.getAvailableLeavesCount()-leave.getCount());
            leaveRepository.save(leave);
            notificationService.notifyLeaveApproved(leave);
        }else{
            throw new LeaveLimitExceededException("Leave Limit Exceeded!");
        }

        return leave;
    }

    @Override
    public Leave denyLeave(Long id) throws LeaveNotFoundException {
        Leave leave = this.getLeave(id);
        leave.setStatus(LeaveStatus.CANCELLED);
        leaveRepository.save(leave);
        return leave;
    }

    @Override
    public Page<Leave> getLeaves(Pageable pageable) {
        return leaveRepository.findAll(pageable);
    }

    @Override
    public Leave getLeave(Long id) throws LeaveNotFoundException {
        Optional<Leave> leave = leaveRepository.findById(id);
        if(!leave.isPresent()){
            throw new LeaveNotFoundException("Leave with this id: " + id + " not found.");
        }
        return leave.get();
    }

    @Override
    public void createLeave(Leave leave) {
        leave.setStatus(LeaveStatus.PENDING);
        leaveRepository.save(leave);
    }

    @Override
    public void updateLeave(Long id, Leave leave) throws LeaveNotFoundException {
        Optional<Leave> existingLeave = leaveRepository.findById(id);
        if(!existingLeave.isPresent()){
            throw new LeaveNotFoundException("Leave with this id: " + leave.getId() + " not found.");
        }
        leaveRepository.save(leave);
    }

    @Override
    public void deleteLeave(Long id) throws LeaveNotFoundException {
        Optional<Leave> leave = leaveRepository.findById(id);
        if(!leave.isPresent()){
            throw new LeaveNotFoundException("Leave with this id: " + id + " not found.");
        }
        leaveRepository.delete(leave.get());
    }

    @Autowired
    public void setLeaveRepository(LeaveRepository leaveRepository) {
        this.leaveRepository = leaveRepository;
    }

    @Autowired
    public void setNotificationService(NotificationService notificationService) {
        this.notificationService = notificationService;
    }
}
