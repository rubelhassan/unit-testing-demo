package com.rubel.unittestingdemo.service.impl;

import com.rubel.unittestingdemo.model.Leave;
import com.rubel.unittestingdemo.service.AccountService;
import com.rubel.unittestingdemo.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author Rubel Hassan
 * @since 11/3/18
 */
@Service
@Transactional
public class NotificationServiceImpl implements NotificationService {

    private AccountService accountService;
    // ... other collaborators

    @Override
    public void notifyLeaveApproved(Leave leave) {
        accountService.notifyLeaveApproved(leave);
        // ... notify other authorities
    }

    @Autowired
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }
}
