package com.rubel.unittestingdemo.service.impl;

import com.rubel.unittestingdemo.model.Leave;
import com.rubel.unittestingdemo.service.AccountService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @author Rubel Hassan
 * @since 11/2/18
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService {
    @Override
    public void notifyLeaveApproved(Leave leave) {
        // intentionally left blank
    }
}
