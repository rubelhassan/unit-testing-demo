package com.rubel.unittestingdemo.service;


import com.rubel.unittestingdemo.exception.LeaveNotFoundException;
import com.rubel.unittestingdemo.exception.LeaveLimitExceededException;
import com.rubel.unittestingdemo.model.Leave;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * @author Rubel Hassan
 * @since 10/30/18
 */
public interface LeaveService {
    Leave approveLeave(Long id) throws LeaveLimitExceededException, LeaveNotFoundException;
    Leave denyLeave(Long id) throws LeaveNotFoundException;
    Page<Leave> getLeaves(Pageable pageable);
    Leave getLeave(Long id) throws LeaveNotFoundException;
    void createLeave(Leave leave);
    void updateLeave(Long id, Leave leave) throws LeaveNotFoundException;
    void deleteLeave(Long id) throws LeaveNotFoundException;
}
