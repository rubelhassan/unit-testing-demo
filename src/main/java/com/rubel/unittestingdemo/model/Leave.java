package com.rubel.unittestingdemo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.rubel.unittestingdemo.utils.LeaveStatus;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;

/**
 * @author Rubel Hassan
 * @since 10/30/18
 */
@Entity
@Table(name = "leaves")
public class Leave {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String reason;

    private Integer count;

    private LocalDate start;

    private LocalDate finish;

    @ManyToOne
    @JoinColumn(name="employee_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Employee employee;

    @Enumerated(EnumType.STRING)
    private LeaveStatus status;

    public Leave(Long id, String reason, Integer count, LocalDate start, LocalDate finish,
                 Employee employee, LeaveStatus status) {
        this.reason = reason;
        this.count = count;
        this.start = start;
        this.finish = finish;
        this.employee = employee;
        this.status = status;
        this.id = id;
    }

    public Leave() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getFinish() {
        return finish;
    }

    public void setFinish(LocalDate finish) {
        this.finish = finish;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public LeaveStatus getStatus() {
        return status;
    }

    public void setStatus(LeaveStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Leave{" +
                "id=" + id +
                ", reason='" + reason + '\'' +
                ", count=" + count +
                ", start=" + start +
                ", finish=" + finish +
                ", employee=" + employee +
                ", status='" + status + '\'' +
                '}';
    }
}
