package com.rubel.unittestingdemo.model;

import javax.persistence.*;
import java.util.List;

/**
 * @author Rubel Hassan
 * @since 10/30/18
 */
@Entity
@Table(name = "employees")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String fullName;

    private Integer availableLeavesCount;

    public Employee(Long id, String fullName, Integer availableLeavesCount) {
        this.fullName = fullName;
        this.availableLeavesCount = availableLeavesCount;
        this.id = id;
    }

    public Employee() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getAvailableLeavesCount() {
        return availableLeavesCount;
    }

    public void setAvailableLeavesCount(Integer availableLeavesCount) {
        this.availableLeavesCount = availableLeavesCount;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", availableLeavesCount=" + availableLeavesCount +
                '}';
    }
}
