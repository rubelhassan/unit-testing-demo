package com.rubel.unittestingdemo.exception;

/**
 * @author Rubel Hassan
 * @since 10/31/18
 */
public class LeaveLimitExceededException extends Exception {

    public LeaveLimitExceededException(String message) {
        super(message);
    }
}
