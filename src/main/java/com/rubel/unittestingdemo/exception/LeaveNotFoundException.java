package com.rubel.unittestingdemo.exception;

/**
 * @author Rubel Hassan
 * @since 10/31/18
 */
public class LeaveNotFoundException extends Exception {

    public LeaveNotFoundException(String message) {
        super(message);
    }
}
