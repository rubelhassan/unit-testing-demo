package com.rubel.unittestingdemo.exception;

import com.rubel.unittestingdemo.resource.error.FieldErrorResource;
import com.rubel.unittestingdemo.resource.error.GlobalErrorResource;
import com.rubel.unittestingdemo.resource.error.ValidationErrorResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;
import java.util.Locale;

/**
 * @author Rubel Hassan
 * @since 11/2/18
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    private MessageSource messageSource;

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> anyException(Exception e) {
        GlobalErrorResource error = new GlobalErrorResource(e.getMessage());
        return new ResponseEntity<>(error, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ValidationErrorResource> validationErrors(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        return new ResponseEntity<>(processFieldErrors(fieldErrors), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    private ValidationErrorResource processFieldErrors(List<FieldError> fieldErrors) {
        ValidationErrorResource validationError = new ValidationErrorResource();

        for (FieldError fieldError: fieldErrors) {
            validationError.addValidationError(resolveErrorMessage(fieldError));
        }

        return validationError;
    }

    private FieldErrorResource resolveErrorMessage(FieldError fieldError) {
        String errorMessage = messageSource.getMessage(fieldError, Locale.ENGLISH);

        if (errorMessage.equals(fieldError.getDefaultMessage())) {
            String[] fieldErrorCodes = fieldError.getCodes();
            if(fieldErrorCodes != null) {
                errorMessage = fieldErrorCodes[0];
            }
        }

        return new FieldErrorResource(fieldError.getField(), errorMessage);
    }

    @Autowired
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }
}
