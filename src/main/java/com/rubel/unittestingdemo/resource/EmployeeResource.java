package com.rubel.unittestingdemo.resource;

import com.rubel.unittestingdemo.controller.EmployeeController;
import com.rubel.unittestingdemo.controller.LeaveController;
import com.rubel.unittestingdemo.model.Employee;
import com.rubel.unittestingdemo.model.Leave;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * @author Rubel Hassan
 * @since 10/31/18
 */
@Relation(value = "employee", collectionRelation = "employees")
public class EmployeeResource extends ResourceSupport {
    private final Employee employee;

    public EmployeeResource(Employee employee) {
        this.employee = employee;
        add(linkTo(EmployeeController.class).withRel("employees"));
        add(linkTo(EmployeeController.class).slash(employee.getId()).withSelfRel());
    }

    public Employee getEmployee() {
        return employee;
    }
}
