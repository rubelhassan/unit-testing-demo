package com.rubel.unittestingdemo.resource.error;

/**
 * @author Rubel Hassan
 * @since 11/2/18
 */
public class FieldErrorResource {
    private String field;
    private String message;

    public FieldErrorResource(String field, String message) {
        this.field = field;
        this.message = message;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
