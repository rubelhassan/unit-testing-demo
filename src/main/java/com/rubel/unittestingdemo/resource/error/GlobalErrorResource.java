package com.rubel.unittestingdemo.resource.error;

import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

/**
 * @author Rubel Hassan
 * @since 11/2/18
 */
@Relation(value = "error")
public class GlobalErrorResource extends ResourceSupport {
    private String message;

    public GlobalErrorResource(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
