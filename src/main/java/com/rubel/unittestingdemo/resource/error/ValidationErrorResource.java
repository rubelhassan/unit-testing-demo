package com.rubel.unittestingdemo.resource.error;

import org.springframework.hateoas.core.Relation;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rubel Hassan
 * @since 11/2/18
 */
@Relation(value = "error")
public class ValidationErrorResource {
    private List<FieldErrorResource> validationErrors = new ArrayList<>();

    public List<FieldErrorResource> getValidationErrors() {
        return validationErrors;
    }

    public void setValidationErrors(List<FieldErrorResource> fieldErrors) {
        this.validationErrors = fieldErrors;
    }

    public void addValidationError(FieldErrorResource error){
        this.validationErrors.add(error);
    }
}
