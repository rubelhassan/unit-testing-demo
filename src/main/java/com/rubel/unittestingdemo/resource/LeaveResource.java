package com.rubel.unittestingdemo.resource;

import com.rubel.unittestingdemo.controller.EmployeeController;
import com.rubel.unittestingdemo.controller.LeaveController;
import com.rubel.unittestingdemo.model.Employee;
import com.rubel.unittestingdemo.model.Leave;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.core.Relation;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * @author Rubel Hassan
 * @since 10/31/18
 */
@Relation(value = "leave", collectionRelation = "leaves")
public class LeaveResource extends ResourceSupport {
    private final Leave leave;

    public LeaveResource(Leave leave) {
        this.leave = leave;
        add(linkTo(LeaveController.class).withRel("leaves"));
        add(linkTo(LeaveController.class).slash(leave.getId()).withSelfRel());
        add(linkTo(EmployeeController.class).slash(leave.getEmployee().getId()).withRel("employee"));
    }

    public Leave getLeave() {
        return leave;
    }
}
