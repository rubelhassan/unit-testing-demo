package com.rubel.unittestingdemo.utils;

/**
 * @author Rubel Hassan
 * @since 10/31/18
 */
public enum LeaveStatus {
    APPROVED, CANCELLED, PENDING;
}
