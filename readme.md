
### Unit Testing Demo Application 
This is a demo application on unit testing using spring framework, JUnit 5, Mockito and Hamcrest.  
  
## Build and Deploy  
The following instruction is for `test` environment. For other environment just change the value of `-Dspring-boot.run.profiles` for command line only and also make changes to `application-{environment}.properties` file to set database credentials and other settings.

### Deploy the application via command-line:  
 - generate application jar
 `mvn clean package`  
 - deploy application jar
`java -jar -Dspring.profiles.active=test ./target/unit-testing-demo-0.0.1-SNAPSHOT.jar`  
  
### Deploy the application via maven plugin:  
`mvn spring-boot:run -Dspring-boot.run.profiles=test`  
  
### Deploy the application  
  Set profile at `application.properties` file and run `UnitTestingDemoApplication` class  
  
### Access application  
 - The application will be running on port `8080` on host machine  
 - Access swagger ui using on [http://localhost:8080/swagger-ui.html#/](http://localhost:8080/swagger-ui.html#/)